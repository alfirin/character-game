<!doctype html>

<html lang="en">
<head>
    <meta charset="utf-8">

    <title>Emagia ~ A Magical Land</title>
    <meta name="description" content="The playground of Orderus">
    <meta name="author" content="Alexandru Leca">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/main.css">
</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center text-white">
                <h1>Welcome to Realm!</h1>
                <h3>This is the home of Heroes, he has been exploring these woods for 100 years and sometimes he has to fight wild beasts to survive.</h3>
                <img src="assets/gfx/orderus.png" alt="" style="width:100%; max-width: 150px;" />
                <div class="info-container">
                    <a class="btn btn-success look-for-trouble" href="javascript:void(0);">Look for trouble!</a>
                </div>
            </div>
        </div>
    </div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/main.js"></script>
</body>
</html>