from SimpleWebSocketServer import SimpleWebSocketServer, WebSocket
from json import loads, dumps

clients = []
class SimpleChat(WebSocket):

    def handleMessage(self):
       client_count = len(clients)
       for client in clients:
          if client != self:
              data = loads(self.data)
              data['message'] = ''
              data['clients'] = client_count
              data = dumps(data)
              client.sendMessage(u'' + data)

    def handleConnected(self):
       print self.address, 'connected'
       for client in clients:
          client.sendMessage(u'{ "message":"' + self.address[0] + u' - connected" }')
       print(len(clients)+1)
       clients.append(self)

    def handleClose(self):
       clients.remove(self)
       print self.address, 'closed'
       print(len(clients))
       for client in clients:
          client.sendMessage(u'{ "message":"' + self.address[0] + u' - disconnected"  }')

server = SimpleWebSocketServer('', 3000, SimpleChat)
server.serveforever()