$(document).ready(function(e){

    $(document).on('click', '.look-for-trouble', function () {
        var target = $('.info-container');
        $.get('ajax.php?action=brawl', function( res ){
            target.empty();
            target.append('<h2><b>'+res.winner+'</b> has defeated <b>' + res.loser + '</b></h2>');
            target.append('<h4>Number of rounds: ' + res.rounds + '</h4>');
            res.roundReports.forEach(function(item, index){
                var round = '<h4>Round #' + (index+1) + '</h4>';
                if ( index+1 == res.roundReports.length ) round = '<h4>Conclusion</h4>';
                target.append('<div class="well well-sm">' + round + '<p>' + item + '</p></div>');
            });
            target.append('<a class="btn btn-success look-for-trouble" href="javascript:void(0);">Look again!</a>');
            $(window).scrollTop(0);
        }, "json")
    });

    $(document).on('click', '.look-for-trouble', function () {
        var data = {
            channel: 1,
            action: 'ready',
            data: {}
        };
        conn.send(JSON.stringify(data));
    });

});

var conn = new WebSocket('ws://' + window.location.host + ':3000');
conn.onopen = function(e) {
    var data = {
        channel: 1,
        action: 'ready',
        data: {}
    };
    conn.send(JSON.stringify(data));
};

conn.onmessage = function(e) {
    var data = JSON.parse(e.data);
    console.log(data);
};