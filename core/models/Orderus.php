<?php
class Orderus extends Hero {

    public function __construct(){
        $name = 'Orderus';
        $health = mt_rand(70, 100);
        $strength = mt_rand(70, 80);
        $defence = mt_rand(45, 55);
        $speed = mt_rand(40, 50);
        $luck = mt_rand(10, 30);
        $skills = array(
            new Skill('Rapid Strike', 2, 10, 'offense'),
            new Skill('Finger of Death', 9001, 1, 'offense'),
            new Skill('Critical Strike', 1.3, 25, 'offense'),
            new Skill('Enchanted Shield', 0.6, 35, 'defense'),
            new Skill('Empowering Shout', 0.3, 10, 'defense'),
            new Skill('Regenerate', 10000, 100, 'offense'),
        );
        parent::__construct($name, $health, $strength, $defence, $speed, $luck, $skills);
    }

}