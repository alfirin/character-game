<?php
class Encounter {

    private $player_one;
    private $player_two;
    private $attacker;
    private $defender;
    private $probability;
    public function __construct($player_one, $player_two){
        $this->player_one = $player_one;
        $this->player_one->stats = $player_one->getStats();
        $this->player_two = $player_two;
        $this->player_two->stats = $player_two->getStats();
        $this->probability = new Probability();
    }

    private function initiativeCheck () {
        /* First we check if player one is slower */
        if ( $this->player_one->stats->speed < $this->player_two->stats->speed ) {
            /* If he is slower then he becomes the defender for the first turn */
            $this->attacker = $this->player_two;
            $this->defender = $this->player_one;
        } else if ( $this->player_one->stats->speed == $this->player_two->stats->speed ) {
            /* If their speed is equal then we check if player one is less lucky */
            if ( $this->player_one->stats->luck < $this->player_two->stats->luck ) {
                /* If he is less fortunate then he becomes the defender for the first turn */
                $this->attacker = $this->player_two;
                $this->defender = $this->player_one;
            } else if ( $this->player_one->stats->speed == $this->player_two->stats->speed ) {
                /* To avoid any other clashes in the off chance that they have equal speed and
                luck it will simply fall to the encounter entry point, so player_one will attack first
                and player two will defend first */
                $this->attacker = $this->player_one;
                $this->defender = $this->player_two;
            } else {
                /* If he is a lucky guy/gal then he becomes the attacker for the first turn */
                $this->attacker = $this->player_one;
                $this->defender = $this->player_two;
            }
        } else {
            /* If he is a fast guy/gal then he becomes the attacker for the first turn */
            $this->attacker = $this->player_one;
            $this->defender = $this->player_two;
        }
    }

    private function resolveRound() {
        $damage = $this->attacker->stats->strength - $this->defender->stats->defence;

        /* If the attacker's strength is lower than the defender's defence then he cannot damage him. */
        if ( $damage < 0 ) {
            /* If he is weaker there is no point going forward for extra checks */
            return (object) array(
                'offenceText' => 'is so insignificant that he strikes',
                'defenceText' => '',
                'damage' => 0
            );
        }

        $offenceText = 'lunges and strikes';
        $defenceText = '';
        /* Check if the attacker is a Hero type and has skills, we check the offensive skills he has
        and stop at the first skill used, because he should not be able to use more than one skill in one turn,
        he might not use any */
        if (method_exists($this->attacker,'getSkills')){
            $offenceSkills = $this->attacker->getSkills()->offence;
            $offenceSkillUsed = false;
            foreach ( $offenceSkills as $skill ) {
                if ($this->probability->proc($skill->chance) && !$offenceSkillUsed) {
                    $damage *= $skill->modifier;
                    $offenceText = 'uses <b style="color: #A90000;">'.$skill->name.'</b> and strikes';
                    $offenceSkillUsed = true;
                }
            }
        }

        /* Check if the defender is a Hero type and has skills, we check the defensive skills he has
        and stop at the first skill used, because he should not be able to use more than one skill in one turn,
        he might not use any */
        $defenceSkillUsed = false;
        if (method_exists($this->defender,'getSkills')){
            $defenceSkills = $this->defender->getSkills()->defence;
            foreach ( $defenceSkills as $skill ) {
                if ($this->probability->proc($skill->chance) && !$defenceSkillUsed) {
                    $damage *= $skill->modifier;
                    $defenceText = 'who uses <b style="color: #009cfc;">'.$skill->name.'</b>';
                    $defenceSkillUsed = true;
                }
            }
        }

        /* Check if the defender get's lucky and dodges or confuses his opponent thus evading all damage */
        if ($this->probability->proc($this->defender->stats->luck)){
            /* If he is lucky, then all damage is set to 0 no matter what the attacker's damage has added up to */
            if ( $defenceSkillUsed ) {
                $defenceText .= ' but also evades the attack taking ';
            } else {
                $defenceText = ' who evades the attack taking ';
            }
            return (object) array(
                'offenceText' => $offenceText,
                'defenceText' => $defenceText,
                'damage' => 0
            );
        } else {
            $defenceText .= ' taking ';
        }

        /* Reduce health of the defender according to damage done */
        $this->defender->stats->health -= $damage;
        return (object) array(
            'offenceText' => $offenceText,
            'defenceText' => $defenceText,
            'damage' => $damage
        );
    }

    private function switchSides() {
        $attacker = $this->defender;
        $defender = $this->attacker;
        $this->attacker = $attacker;
        $this->defender = $defender;
    }

    public function brawl() {
        /* First we check who goes first */
        $this->initiativeCheck();
        $round = 0;
        $result = (object) array(
            'winner' => '',
            'rounds' => 0,
            'roundReports' => array(),
            'loser' => ''
        );

        /* Cycle back and forth untill one of the entities has no more health, or they end round 20 */
        while($this->attacker->stats->health > 0 && $this->defender->stats->health > 0 && $round < 20) {
            $round++;
            /* Resolve combat for the current round */
            $damage = $this->resolveRound();

            $outputText = '<b>%s</b> (%d HP) %s <b>%s</b> (%d HP) %s <b>%d</b> points of damage.';
            $roundText = sprintf(
                $outputText,
                $this->attacker->getName(),
                $this->attacker->stats->health,
                $damage->offenceText, $this->defender->getName(),
                $this->defender->stats->health,
                $damage->defenceText,
                $damage->damage);
            array_push($result->roundReports, $roundText);

            /* Trade places */
            $this->switchSides();
        }

        /* Declare Winner */
        if ($this->attacker->stats->health > $this->defender->stats->health) {
            $result->winner = $this->attacker->getName();
            $result->loser = $this->defender->getName();
        } else {
            $result->winner = $this->defender->getName();
            $result->loser = $this->attacker->getName();
        }

        $result->rounds = $round;

        array_push($result->roundReports, '<b>' . $result->loser . '</b> collapses to the ground, he was no match for the mighty <b>' . $result->winner . '</b>');

        /* Return an object with encounter data. */
        return $result;
    }

}