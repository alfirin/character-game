<?php
class Entity{

    private $name;
    private $health;
    private $strength;
    private $defence;
    private $speed;
    private $luck;

    public function __construct($name, $health, $strength, $defence, $speed, $luck){
        $this->name = $name;
        $this->health = $health;
        $this->strength = $strength;
        $this->defence = $defence;
        $this->speed = $speed;
        $this->luck = $luck;
    }

    public function getName() {
        return $this->name;
    }

    public function getStats() {
        return (object) array(
            'health' => $this->health,
            'strength' => $this->strength,
            'defence' => $this->defence,
            'speed' => $this->speed,
            'luck' => $this->luck
        );
    }

}