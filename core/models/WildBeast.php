<?php
class WildBeast extends Entity {

    public function __construct(){
        $rand_word = rand_word::getInstance();
        $name = 'The ' . $rand_word->generate(10, false, true) . ' Beast';
        $health = mt_rand(60, 90);
        $strength = mt_rand(60, 90);
        $defence = mt_rand(40, 60);
        $speed = mt_rand(40, 60);
        $luck = mt_rand(25, 40);
        parent::__construct($name, $health, $strength, $defence, $speed, $luck);
    }

}