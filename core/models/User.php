<?php
class User{

    private $id;
    public $username;
    public $first_name;
    public $last_name;
    public $gender;
    public $email;
    public $character;

    public function __construct($id, $first_name, $last_name, $gender, $email){
        $this->id = $id;
        $this->first_name = $first_name;
        $this->last_name = $last_name;
        $this->gender = $gender;
        $this->email = $email;
        $this->username = 'Anonymous';
        if ( isset($_SESSION['user']->character) ){
            $this->character = $_SESSION['user']->character;
        }
    }

    public function getId() {
        return $this->id;
    }

    private function updateSession () {
        $_SESSION["user"] = $this;
    }

    public function setCharacter( $character ) {
        $_SESSION["user"] = $this;
        $_SESSION["user"]->character = $character;
    }

}