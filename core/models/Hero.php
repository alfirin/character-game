<?php
class Hero extends Entity {

    private $skills;

    public function __construct($name, $health, $strength, $defence, $speed, $luck, $skills){
        parent::__construct($name, $health, $strength, $defence, $speed, $luck);
        $this->skills = $skills;
    }

    public function getSkills() {
        $defence = array();
        $offence = array();
        foreach ( $this->skills as $skill ) {
            if ( $skill->type == 'offense' ) {
                array_push($offence, $skill);
            } else {
                array_push($defence, $skill);
            }
        }
        $chanceSortFunc = function ($a, $b)
        {
            return $a->chance > $b->chance;
        };
        usort($offence, $chanceSortFunc);
        usort($defence, $chanceSortFunc);
        return (object) array(
            'offence' => $offence,
            'defence' => $defence,
        );
    }

}