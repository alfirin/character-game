<?php
class Skill {

    public $name;
    public $modifier;
    public $chance;
    public $type;

    public function __construct($name, $modifier, $chance, $type){
        $this->name = $name;
        $this->modifier = $modifier;
        $this->chance = $chance;
        $this->type = $type;
    }

}