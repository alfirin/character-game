<?php
class SessionManager{

    public $user;

    public function __construct($id = 0){
        if ( session_id() && !isset($_SESSION['user']) ){
            $newUser = new User($id, 'Alexandru', 'Leca', 'male', 'alexandru.leca89@gmail.com');
            $orderus = new Orderus();
            $newUser->setCharacter($orderus);
            $_SESSION['user'] = serialize($newUser);
        } else {
            $sessionUser = unserialize($_SESSION['user']);
            if ( !isset($sessionUser->character) ){
                $sessionUser->character = new Orderus();
                $_SESSION['user'] = serialize($sessionUser);
            }
        }
    }

}