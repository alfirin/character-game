<?php
class AjaxManager{

    public $request;
    public $request_method;
    public $data;

    public function __construct(){
        $this->request = $_REQUEST;
        $this->request_method = $_SERVER["REQUEST_METHOD"];
        switch($_SERVER['REQUEST_METHOD'])
        {
            case 'GET':
                $this->data = &$_GET;
                $this->solveGetRequest();
                break;
            case 'POST':
                $this->data = &$_POST;
                $this->solvePostRequest();
                break;
            case 'PUT':
                $this->data = &$_POST;
                $this->solvePutRequest();
                break;
            case 'DELETE':
                $this->data = &$_POST;
                $this->solveDeleteRequest();
                break;
            case 'OPTIONS':
                $this->data = &$_POST;
                $this->solveOptionsRequest();
                break;
            default:
        }
    }

    private function solveGetRequest() {
        if ( $this->data['action'] == 'brawl' ){ $this->doBrawl(); }
    }

    private function solvePostRequest() {
        if ( $this->data['action'] == 'authorize' ){ $this->authorize(); }
    }
    private function solvePutRequest() {}
    private function solveDeleteRequest() {}
    private function solveOptionsRequest() {}

    private function authorize () {

    }

    private function doBrawl () {
        $user = unserialize($_SESSION['user']);

        $beast = new WildBeast();
        $encounter = new Encounter( $user->character, $beast );

        echo json_encode($encounter->brawl());
    }

}