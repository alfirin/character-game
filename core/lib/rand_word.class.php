<?php

/**
* @author		Eric Sizemore <admin@secondversion.com>
* @package		Random Word
* @version		1.0.2
* @copyright	2006 - 2011 Eric Sizemore
* @license		GNU GPL
* 
*	This program is free software; you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation; either version 2 of the License, or
*	(at your option) any later version.
*
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
*	GNU General Public License for more details.
*/

// Slightly inspired by class randomWord by kumar mcmillan
class rand_word
{
	/**
	* Class instance
	*/
	private static $instance;

	/**
	* All vowels
	* 
	* @var array
	*/
	private $vowels = array('a', 'e', 'i', 'o', 'u', 'y');

	/**
	* All consonants
	*
	* @var array
	*/
	private $consonants = array(
		'b', 'c', 'd', 'f', 'g', 'h', 
		'j', 'k', 'l', 'm', 'n', 'p', 
		'r', 's', 't', 'v', 'w', 'z', 
		'ch', 'qu', 'th', 'xy'
	);

	/**
	* Resulting word
	*
	* @var string
	*/
	private $word = '';

	/**
	* Constructor.
	*/
	private function __construct() {}

	/**
	* Creates an instance of the class.
	*
	* @param  void
	* @return object
	*/
	public static function getInstance()
	{
		if (!self::$instance)
		{
			self::$instance = new self();
		}
		return self::$instance;
	}

	/**
	* Generates the random word.
	*
	* @param	integer		$length			Length of the word
	* @param	boolean		$lower_case		Return the word lowercase?
	* @param	boolean		$ucfirst		Return the word with the first letter capitalized?
	* @param	boolean		$upper_case		Return the word uppercase?
	* @return	string
	*/
	public function generate($length = 5, $lower_case = true, $ucfirst = false, $upper_case = false)
	{
		$done = false;
		$const_or_vowel = 1;

		while (!$done)
		{
			switch ($const_or_vowel)
			{
				case 1:
					$this->word .= $this->consonants[array_rand($this->consonants)];
					$const_or_vowel = 2;
					break;
				case 2:
					$this->word .= $this->vowels[array_rand($this->vowels)];
					$const_or_vowel = 1;
					break;
			}

			if (strlen($this->word) >= $length)
			{
				$done = true;
			}
		}

		$this->word = substr($this->word, 0, $length);

		if ($lower_case)
		{
			$this->word = strtolower($this->word);
		}
		else if ($ucfirst)
		{
			$this->word = ucfirst(strtolower($this->word));
		}
		else if ($upper_case)
		{
			$this->word = strtoupper($this->word);
		}
		return $this->word;
	}
}

?>